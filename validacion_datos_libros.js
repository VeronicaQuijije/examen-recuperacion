
const codigo  = document.getElementById("codigo");
const titulo = document.getElementById("titulo");
const autor = document.getElementById("autor");
const editorial = document.getElementById("editorial");
const año = document.getElementById("año");
const fechainicial = document.getElementById("fechainicial");
const fechafinal = document.getElementById("fechafinal");


const validarcodigo= new RegExp("[A-Za-z0-9]{5}+");
const validartitulo= new RegExp("[A-Za-z0-9]{5}+");
const validarautor= new RegExp("[A-Za-z0-9]{5}+");

const validaraño = /^\d+$/gi;
const validarfechafinal =new RegExp("/^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/");
const validarfechainicio = new RegExp("/^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/");

formulario.addEventListener("submit", (e)=>{
e.preventDefault();

if(!validarcodigo.test(codigo) || !validaraño.test(año) || !validartitulo.test(titulo) || !validarautor.test(autor) || !validarfechafinal.text(fechafinal) || !validarfechainicio.test(fechainicial)) {
    console.log("Datos no cumplen con un campo válido")
    return;

} })
 